#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import json
from optparse import OptionParser
import datetime
from collections import namedtuple, defaultdict
import gzip
from string import Template

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log"
}

report = {
    'count': 0,
    'count_perc': 0,
    'time_sum': 0,
    'time_perc': 0,
    'time_avg': 0,
    'time_max': 0,
    'time_med': 0,

}


def create_report_path(config, log_date):
    """Create report file, if already exists - return None"""
    report_dir = config['REPORT_DIR']
    report_name = 'report-{}.html'.format(
        datetime.datetime.strftime(log_date, '%Y.%m.%d'))

    if not os.path.isdir(report_dir):
        os.makedirs(report_dir)

    report_path = os.path.join(report_dir, report_name)

    if os.path.exists(report_path):
        return None

    return report_path


def fetch_last_log(log_directory):
    """Fetch last log's relative path, date and extension.

    If there is no logs return None
    """
    all_files = os.listdir(log_directory)

    log_name_regex = re.compile(r'nginx-access-ui\.log-(\d{8})\.?(gz)?')

    last_date = last_log_name = last_log_extension = ''

    for file_name in all_files:
        match = log_name_regex.match(file_name)

        if match:
            log_date = match.group(1)
            if log_date > last_date:
                last_date = log_date
                last_log_name = file_name
                last_log_extension = match.group(2)

    if last_date:

        last_log = namedtuple('LastFile', ['path', 'date', 'extension'])

        last_log.path = os.path.join(log_directory, last_log_name)
        last_log.date = datetime.datetime.strptime(last_date, '%Y%m%d')
        last_log.extension = last_log_extension

        return last_log


def parse_line(log, critical_error_percent=30):
    """Create generator for log parsing.

    Open log file, iterate over log lines and yield url, request time and
    overall count of successfully parsed lines and errored lines.
    """
    log_line_regex = re.compile(r'"\w+? ([^"]+?) [^"]+?" .+?(\d+\.\d+)$')

    parsing_errors = parsed = 0

    with gzip.open(log.path) if log.extension == 'gz' else open(log.path) as log_file:
        for log_line in log_file:
            match = log_line_regex.search(log_line)

            if not match:
                parsing_errors += 1
                continue

            url = match.group(1)
            request_time = float(match.group(2))

            parsed += 1

            yield url, request_time

    if not parsed:
        print 'Feed cannot be parsed from {}'.format(log.path)
        raise Exception('Feed is not parsible.')

    error_percent = calc_percent(parsing_errors, parsed)

    if error_percent >= critical_error_percent:
        print 'Error percent during log parsing is too high: {}'.format(error_percent)
        raise Exception('Too many errors during parsing.')


def collect_nginx_report(parser_generator):
    """Aggregate statistic for each url from parsing generator.

    Statistic format:

        {
            'url': [<request_time>, ..., <request_time>],
             ...
            'url': [<request_time>, ..., <request_time>],
    }
    """
    urls_count_total = reqtime_total = 0
    urls_report = defaultdict(list)

    for url, request_time in parser_generator:
        urls_report[url].append(request_time)
        urls_count_total += 1
        reqtime_total += request_time

    nginx_report = namedtuple('NginxReport', ['urls_report', 'urls_count_total', 'reqtime_total'])

    nginx_report.urls_report = urls_report
    nginx_report.urls_count_total = urls_count_total
    nginx_report.reqtime_total = reqtime_total

    return nginx_report


def calc_request_stat(nginx_report, config):
    """Aggregate statistic for url's and request time in single list.

    Collect statistic for each url in dictionary, append the dictionary to the
    list with statistic about all urls.
    """
    urls_stat_all = []
    report_size = config['REPORT_SIZE']

    for url, reqtimes in nginx_report.urls_report.items():

        url_count = len(reqtimes)
        url_reqtime = sum(reqtimes)

        url_stat = {
            'url': url,
            'count': url_count,
            'count_perc': calc_percent(url_count, nginx_report.urls_count_total),
            'time_sum': round(url_reqtime, 3),
            'time_perc': calc_percent(url_reqtime, nginx_report.reqtime_total),
            'time_avg': round(url_reqtime / url_count, 3),
            'time_max': max(reqtimes),
            'time_med': round(calc_median(reqtimes), 3),
        }
        urls_stat_all.append(url_stat)

    urls_stat_all.sort(key=lambda x: x['time_sum'], reverse=True)

    return urls_stat_all[:report_size]


def calc_percent(part, whole):
    """Calculate percent."""
    return round(float(part) / whole * 100, 3)


def calc_median(values):
    """Calculate median by values."""
    values_count = len(values)

    if values_count == 2:
        return sum(values) / 2

    if values_count == 1:
        return values[0]

    if values_count > 2:

        values.sort()

        if not values_count % 2:
            first_half = values_count / 2
            second_half = values_count / 2 - 1

            return (values[first_half] + values[second_half]) / 2

        return values[values_count // 2]


def extract_config_from_file(path):
    """Extract config info from file."""
    if not os.path.isfile(path):
        print 'No config file by path {}'.format(path)
        raise Exception('Please provide valid config path.')

    with open(path) as config_file:
        if config_file.readline():
            config_file.seek(0)
            return json.load(config_file)

        return {}


def merge_config(default_config, custom_config=None):
    """Create single config from two configs."""
    if not custom_config:
        custom_config = {}

    config_final = {}

    for key in default_config:
        config_final[key] = custom_config.get(key, None) or default_config[key]

    return config_final


def fetch_config_path():
    """Extract config path from script arguments."""
    arg_parser = OptionParser()
    arg_parser.add_option('--config', dest='config')

    return arg_parser.parse_args()[0].config


def validate_config(config):
    """Validate config file"""
    try:
        valid_report_size = str(config.get('REPORT_SIZE'))
    except ValueError:
        valid_report_size = False

    valid_log_dir = os.path.isdir(config.get('LOG_DIR'))

    return valid_report_size and valid_log_dir


def output_report(report_path, stat):
    """Write report to the file."""
    with open(report_path, mode='w') as report:
        with open('report.html') as tmpl:
            t = Template(tmpl.read())
            report_done = t.safe_substitute(table_json=stat)

            report.write(report_done)


def main(config):
    last_log = fetch_last_log(config['LOG_DIR'])

    if not last_log:
        print 'There is no logs in folder {}'.format(config['LOG_DIR'])
        return

    report_path = create_report_path(config, last_log.date)

    if not report_path:
        print 'Log {} is already reported'.format(last_log.path)
        return

    parser_generator = parse_line(last_log)

    nginx_report = collect_nginx_report(parser_generator)

    stat = calc_request_stat(nginx_report, config)

    output_report(report_path, stat)


if __name__ == "__main__":
    config_path = fetch_config_path()

    if config_path:
        file_config = extract_config_from_file(config_path)
        config = merge_config(config, file_config)

    if not validate_config(config):
        print 'Config is not valid'
        raise Exception

    try:
        main(config)
    except Exception as e:
        print 'Log out with Exception {}'.format(e)
